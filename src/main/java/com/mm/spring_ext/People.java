package com.mm.spring_ext;

import java.util.List;
import java.util.Map;

public class People {
	private String id;
	private String name;
	private Integer age;
	private List<String> own;
	
	private Map<String,String> myMap;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public List<String> getOwn() {
		return own;
	}

	public void setOwn(List<String> own) {
		this.own = own;
	}

	public Map<String, String> getMyMap() {
		return myMap;
	}

	public void setMyMap(Map<String, String> myMap) {
		this.myMap = myMap;
	}

}
