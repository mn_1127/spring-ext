package com.mm.spring_ext;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class MyNamespaceHandler extends NamespaceHandlerSupport {
	public void init() {
		registerBeanDefinitionParser("student", new PeopleBeanDefinitionParser());
	}
}
