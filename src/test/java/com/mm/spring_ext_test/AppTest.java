package com.mm.spring_ext_test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mm.spring_ext.People;

public class AppTest {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("test.xml");
		People f = (People) ctx.getBean("scf1");
		if (f != null && f.getOwn() != null && f.getOwn().size() > 0) {
			for (String own : f.getOwn()) {
				System.out.println(own);
			}
		}
	}
}
