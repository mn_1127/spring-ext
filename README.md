## spring-ext

spring-ext 是一个自定义spring的配置标签的实例，网上已经有很多自定义spring配置的标签的例子，

但是都是以简单的以标签属性为String类型的方式注入到bean的简单属性的，当需要注入一个list或者map时，网上基本上找不到对应的例子。

因为自己有这个需求，所以就自己写了一个列子，今天在此托管，以备忘！


## 总结：
#### 要自定义Spring的配置标签，需要一下几个步骤：
#### 1.使用XSD定义XML配置中标签元素的结构(scf.XSD)
#### 2.提供该XSD命名空间的处理类，它可以处理多个标签定义(MyNamespaceHandler.java)
#### 3.为每个标签元素的定义提供解析类。(PeopleBeanDefinitionParser.java)
#### 4.两个特殊文件通知Spring使用自定义标签元素（spring.handlers 和spring.schemas)


